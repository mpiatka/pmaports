# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdoctools
pkgver=5.55.0
pkgrel=0
pkgdesc="Documentation generation from docbook"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends="docbook-xsl"
depends_dev="qt5-qtbase-dev ki18n-dev karchive-dev libxslt-dev libxml2-dev libxml2-utils docbook-xml docbook-xsl"
makedepends="$depends_dev extra-cmake-modules perl-uri"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Broken, complains about not being able to find "kdoctools catalogs"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIB_INSTALL_DIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}


package() {
	cd "$builddir"
	make DESTDIR="${pkgdir}" install
}

sha512sums="157f7b98b16b8724c66885a2512952230ca8492b2f495d3738f1f4793c24c94b05751c88d4c4c79b3bd942b70e33e5fecb56f855383d883ef1b2f0c1ea8007d1  kdoctools-5.55.0.tar.xz"
